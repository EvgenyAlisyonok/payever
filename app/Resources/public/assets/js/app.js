var ModalRegion, app;

app = new Marionette.Application;

app.addRegions({
  settings: '#app',
  modal: '#modal'
});

app.on('start', function() {
  if (Backbone.history) {
    Backbone.history.start();
  }
});

ModalRegion = Backbone.Marionette.Region.extend({
  el: '#modal',
  constructor: function() {
    _.bindAll(this);
    Backbone.Marionette.Region.prototype.constructor.apply(this, arguments);
    this.on('view:show', this.showModal, this);
  },
  getEl: function(selector) {
    var $el;
    $el = $(selector);
    $el.on('hidden', this.close);
    return $el;
  },
  showModal: function(view) {
    view.on('close', this.hideModal, this);
    this.$el.modal('show');
  },
  hideModal: function() {
    this.$el.modal('hide');
  }
});

$(document).ready(function() {
  console.log('App.init');
  app.start();
});

var Controller, SettingsController;

SettingsController = Marionette.Controller.extend({
  onSettingsRoute: function() {
    var user;
    user = new UserModel();
    user.fetch({
      success: function(user) {
        alert(JSON.stringify(user));
      }
    });
    return app.settings.show(settingsView);
  }
});

Controller = new SettingsController;

var myRouter;

myRouter = new Marionette.AppRouter({
  controller: Controller,
  appRoutes: {
    '': 'onSettingsRoute'
  }
});

var UserModel;

UserModel = Backbone.Model.extend({
  urlRoot: '/user',
  defaults: {
    userName: '',
    userEmail: '',
    siteUrl: '',
    userBirthday: {
      year: '',
      month: '',
      day: ''
    },
    userGender: '',
    userPhone: '',
    userSkill: '',
    userAbout: '',
    password: ''
  }
});

var ModalView;

ModalView = Marionette.LayoutView.extend({
  template: '#modal-settings-view',
  events: {
    'click .btn-cancel': 'onCancelClick',
    'click .btn-save': 'onSaveClick',
    'blur .form-control': 'toggleClass'
  },
  onCancelClick: function() {
    var $modalEl;
    console.log('cancel');
    $modalEl = $('#modal');
    return $modalEl.html('');
  },
  onSaveClick: function() {
    console.log('save');
  },
  toggleClass: function() {
    var $target;
    $target = $(event.target);
    if ($target.val().length > 0) {
      $target.addClass('active');
    } else {
      $target.removeClass('active');
    }
    console.log($target.val());
  }
});

var SettingsView, settingsView;

SettingsView = Marionette.LayoutView.extend({
  template: '#settings-view',
  events: {
    'click .btn': 'onClickButton'
  },
  onClickButton: function() {
    var $modalEl, modalView;
    modalView = new ModalView;
    modalView.render();
    $modalEl = $('#modal');
    $modalEl.html(modalView.el);
  }
});

settingsView = new SettingsView;

settingsView.render();
