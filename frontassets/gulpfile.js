'use strict';

var gulp = require('gulp'),
    jade = require('gulp-jade'),
    coffee = require('gulp-coffee'),
    sass = require('gulp-sass'),
    concat = require ('gulp-concat'),
    sourceMaps = require('gulp-sourcemaps'),
    autoPrefixer = require('gulp-autoprefixer'),
    imageMin = require('gulp-imagemin'),
    newer = require('gulp-newer'),
    plumber = require('gulp-plumber'),
    prettify = require('gulp-html-prettify'),
    browserSync = require('browser-sync').create(),
    spriteSmith = require('gulp.spritesmith');

var path = {
    vendor: {
        scripts: [
            './bower_components/jquery/dist/jquery.js',
            './bower_components/underscore/underscore.js',
            './bower_components/backbone/backbone.js',
            './bower_components/backbone.marionette/lib/backbone.marionette.js',
            './bower_components/bootstrap-sass/assets/javascripts/bootstrap.js',
        ],
        fonts: []
    }
};

gulp.task('sprite', function () {
  gulp.src('./assets/img/sprite/*.png')
    .pipe(spriteSmith({
      padding: 20,
      imgPath: '../img/sprite.png',
      imgName: 'sprite.png',
      cssName: 'sprite.scss'
    }))
    .pipe(gulp.dest('./assets/img/'));
});
gulp.task('images', function () {
    gulp.src(['./assets/img/*.png', './assets/img/*.gif', './assets/img/*.svg'])
        .pipe(plumber())
        .pipe(newer('../app/Resources/public/img/'))
        .pipe(imageMin({
            progressive: true,
            interlaced: true,
            svgoPlugins: [{removeViewBox: false}, {removeUselessStrokeAndFill: false}]
        }))
        .pipe(gulp.dest('../app/Resources/public/assets/img/'));
});

gulp.task('styles', function () {
    gulp.src('./assets/scss/common.scss')
        .pipe(plumber())
        .pipe(sourceMaps.init())
        .pipe(sass())
        .pipe(autoPrefixer({
            browsers: ['> 1%', 'last 4 versions', 'Firefox ESR', 'Opera 12.1'],
            cascade: false
        }))
        .pipe(sourceMaps.write('.'))
        .pipe(gulp.dest('../app/Resources/public/assets/css/'));
});

gulp.task('jade', function () {
    gulp.src('./assets/template/*.jade')
        .pipe(plumber())
        .pipe(jade({
            pretty: true
        }))
        .pipe(prettify({indent_char: ' ', indent_size: 4}))
        .pipe(gulp.dest('../app/Resources/public/html/'));
});

gulp.task('templates', function () {
    gulp.src('./assets/coffee/templates/**/*.jade')
        .pipe(plumber())
        .pipe(jade({
            pretty: true
        }))
        .pipe(prettify({indent_char: ' ', indent_size: 4}))
        .pipe(gulp.dest('../app/Resources/public/assets/templates'));
});

gulp.task('scripts', function () {
    gulp.src(path.vendor.scripts)
        .pipe(plumber())
        .pipe(gulp.dest('../app/Resources/public/assets/js/'));
    gulp.src('./assets/coffee/**/*.coffee')
        .pipe(plumber())
        .pipe(coffee({bare: true}))
        .pipe(concat('app.js'))
        .pipe(gulp.dest('../app/Resources/public/assets/js/'));
});

gulp.task('fonts', function () {
    gulp.src('./assets/fonts/**/*')
        .pipe(plumber())
        .pipe(newer('./assets/fonts/**/*'))
        .pipe(gulp.dest('../app/Resources/public/assets/fonts/'));
});

gulp.task('bs', function () {
    browserSync.init({
        server: '../app/Resources/public',
        directory: true,
        open: false,
        notify: false,
        reloadOnRestart: false,
        ghostMode: false,
        startPath: 'html'
    });
});

gulp.task('watch', function () {
    gulp.watch(['./assets/img/sprite.scss', './assets/scss/**/*.scss'], ['styles']);
    gulp.watch('./assets/template/**/*.jade', ['jade']);
    gulp.watch('./assets/coffee/templates/**/*.jade', ['templates']);
    gulp.watch('./assets/coffee/**/*.coffee', ['scripts']);
    gulp.watch('./assets/img/sprite/*', ['sprite']);
    gulp.watch(['./assets/img/*.png', './assets/img/*.gif', './assets/img/*.svg'], ['images']);
    gulp.watch('./assets/fonts/**/*', ['fonts']);
});

gulp.task('build', ['sprite', 'styles', 'jade', 'templates', 'scripts', 'images', 'fonts']);
gulp.task('default', ['build', 'watch', 'bs']);
