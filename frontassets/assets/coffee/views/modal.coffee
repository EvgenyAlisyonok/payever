ModalView = Marionette.LayoutView.extend(
  template: '#modal-settings-view',

  events:
    'click .btn-cancel': 'onCancelClick',
    'click .btn-save': 'onSaveClick',
    'blur .form-control': 'toggleClass'
  onCancelClick: ->
    console.log('cancel');

    $modalEl = $('#modal');
    $modalEl.html('');

  onSaveClick: ->
    console.log('save');
    return

  toggleClass: ->
    $target = $(event.target);
    if $target.val().length > 0 then $target.addClass('active') else $target.removeClass('active')
    console.log($target.val())
    return
)
