SettingsView = Marionette.LayoutView.extend(
  template: '#settings-view',

  events: 'click .btn': 'onClickButton'
  onClickButton: ->
    modalView = new ModalView
    modalView.render()

    $modalEl = $('#modal')
    $modalEl.html modalView.el
    return
)

settingsView = new SettingsView
settingsView.render()

