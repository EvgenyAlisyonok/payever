UserModel = Backbone.Model.extend(
  urlRoot: '/user'
  defaults:
    userName: ''
    userEmail: ''
    siteUrl: ''
    userBirthday:
      year: ''
      month: ''
      day: ''
    userGender: ''
    userPhone: ''
    userSkill: ''
    userAbout: ''
    password: '')
