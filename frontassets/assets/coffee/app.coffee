app = new (Marionette.Application)

app.addRegions
  settings: '#app',
  modal: '#modal'

app.on 'start', ->
  if Backbone.history
    Backbone.history.start()
  return

ModalRegion = Backbone.Marionette.Region.extend(
  el: '#modal'
  constructor: ->
    _.bindAll this
    Backbone.Marionette.Region::constructor.apply this, arguments
    @on 'view:show', @showModal, this
    return
  getEl: (selector) ->
    $el = $(selector)
    $el.on 'hidden', @close
    $el
  showModal: (view) ->
    view.on 'close', @hideModal, this
    @$el.modal 'show'
    return
  hideModal: ->
    @$el.modal 'hide'
    return
)

$(document).ready ->
  console.log 'App.init'
  app.start();
  return
